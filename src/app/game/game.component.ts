import { Component, OnInit} from '@angular/core';
//import { FilesService } from '../files.service';
import { GameService } from '../game.service';
import { Game } from '../game';
import {ModalService} from '../modal.service'

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  private bodyText: string;
  gameName = '';
  newvideoGame = ''; 
  description = '';
  selectedFile = ''; 
  idN : number;

  constructor(private gameService: GameService,private modalService: ModalService) { }

  //gameList = ['League of legends','Call of Duty', 'Fifa']
  
  gameList: Game[] = this.gameService.gameListService

  ngOnInit() {
    this.bodyText = 'This text can be updated in modal 1';
  }

  gameResult = this.gameList;

  onKey(event: any) { // without type info
    this.gameResult = this.gameList
    this.gameName = event.target.value;
    if(this.gameName !== ''){
      let regex = new RegExp(this.gameName,"i")
      this.gameResult = this.gameList.filter(game => {
        return regex.test(game.name)
      });
    }  
  }

  onFileChanged(event) {       
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.selectedFile = reader.result.toString()
        console.log(this.selectedFile)
      };
    }    
  }

  onAddVideoGame(id: string){
    console.log(this.newvideoGame,this.description,this.selectedFile)
    this.gameService.onAddVideoGame(this.newvideoGame,this.description,this.selectedFile)
    this.modalService.close(id);
    this.emptyVariable()
  }

  onDeleteVideoGame(id: number)
  {   
    console.log(id)
    let result = this.gameService.onDeleteVideoGame(id)
  }

  onModifyVideoGame(id: string)
  {  
    console.log(this.idN)     
    this.gameService.onModifyVideoGame(this.idN,this.newvideoGame,this.description,this.selectedFile)
    this.modalService.close(id);
    this.emptyVariable()
  }

  openModal(id: string, idN : number = null) {    
    this.modalService.open(id);
    if(idN !== null){
      this.newvideoGame = this.gameList[idN].name
      this.description = this.gameList[idN].content
      this.selectedFile = this.gameList[idN].url
      this.idN = idN
    }
  }

  closeModal(id: string) {    
     this.modalService.close(id);
     this.emptyVariable()
  }

  emptyVariable(){
    this.newvideoGame = ''
    this.description = ''
    this.selectedFile = ''
  }
}
