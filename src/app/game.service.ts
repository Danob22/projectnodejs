import { Injectable } from '@angular/core';
import { Game } from './game';

@Injectable()
export class GameService {
  gameListService : Game[]=[
    {
        id: 1,
        name: 'League of legends',
        content: 'Game 1',
        url : '../assets/lol.jpg'
    },
    {
        id: 2,
        name: 'Call of Duty',
        content: 'Game 2',
        url : '../assets/cod.jpg'
    },
    {
        id: 3,
        name: 'Fifa',
        content: 'Game 3',
        url : '../assets/fifa.jpg'
    },
  ]
  constructor() { }

  onAddVideoGame(newvideoGame: string, description: string, selectedFile: string){
    let id = this.gameListService[this.gameListService.length - 1].id
    id = id + 1
    this.gameListService.push({id: id, name: newvideoGame, content: description, url:selectedFile})
  }

  onDeleteVideoGame(id: number)
  {    
    if(id !== -1){
      return this.gameListService.splice(id,1)        
    }
  }

  onModifyVideoGame(id: number, newvideoGame: string, description: string, selectedFile:string)
  {
    this.gameListService[id].name = newvideoGame;
    this.gameListService[id].content = description;
    this.gameListService[id].url = selectedFile;
    console.log(this.gameListService)
  }

}
