import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';// ngModel

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GameComponent } from './game/game.component';

import { GameService } from './game.service';
import { ModalComponent } from './modal/modal.component';
import {ModalService} from './modal.service'


@NgModule({
  declarations: [
    AppComponent,
    GameComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [GameService,ModalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
